package com.gitlab.d161tal.server.controller.initiative

import com.gitlab.d161tal.server.api.v1.initiative.Initiative
import com.gitlab.d161tal.server.dto.InitiativeDTO
import com.gitlab.d161tal.server.service.initiative.InitiativeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/initiatives")
class InitiativeController(@Autowired private val initiativeService: InitiativeService) {
    @GetMapping
    fun getInitiatives() = initiativeService.list()

    @GetMapping("/{id}")
    fun getInitiative(@PathVariable id: Long) = initiativeService.get(id)

    @PostMapping
    fun addInitiative(@RequestBody initiative: InitiativeDTO): Initiative {
        val persistentInitiative = Initiative().apply {
            title = initiative.title
            description = initiative.description
            date = initiative.date
        }

        val savedSpecs = initiativeService.addSpecs(initiative.type.create())
        persistentInitiative.initiativeSpecs = savedSpecs
        return initiativeService.add(persistentInitiative)
    }

    @DeleteMapping("/{id}")
    fun deleteInitiative(@PathVariable id: Long) = initiativeService.remove(id)
}
