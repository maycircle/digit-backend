package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.api.v1.Notification
import com.gitlab.d161tal.server.dto.NotificationDTO
import com.gitlab.d161tal.server.service.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/notifications")
class NotificationController(@Autowired private val notificationService: NotificationService) {
    @GetMapping
    fun getNotifications() = notificationService.list()

    @GetMapping("/{id}")
    fun getNotification(@PathVariable id: Long) = notificationService.get(id)

    @PostMapping
    fun addNotification(@RequestBody notification: NotificationDTO): Notification {
        val persistentNotification = Notification().apply{
            title = notification.title
            startDate = notification.startDate
            endDate = notification.endDate
            description = notification.description
        }

        return notificationService.add(persistentNotification)
    }
}