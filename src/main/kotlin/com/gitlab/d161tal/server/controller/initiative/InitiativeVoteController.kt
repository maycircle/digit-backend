package com.gitlab.d161tal.server.controller.initiative

import com.gitlab.d161tal.server.api.v1.initiative.Initiative
import com.gitlab.d161tal.server.api.v1.initiative.VoteOption
import com.gitlab.d161tal.server.dto.VoteDTO
import com.gitlab.d161tal.server.service.initiative.InitiativeService
import com.gitlab.d161tal.server.service.initiative.VoteInitiativeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/initiatives/{id}/votes")
class InitiativeVoteController(@Autowired private val initiativeService: InitiativeService,
                               @Autowired private val voteInitiativeService: VoteInitiativeService
) {
    @PostMapping
    fun addVote(@PathVariable id: Long, @RequestBody vote: VoteDTO): Initiative {
        val initiative = initiativeService.get(id).get()
        for (item in vote.options) {
            voteInitiativeService.addOption(VoteOption().apply {
                voteInitiative = initiative.initiativeSpecs
                optionName = item
            })
        }

        return initiative
    }
}
