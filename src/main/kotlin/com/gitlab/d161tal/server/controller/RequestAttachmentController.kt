package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.api.v1.FileAttachment
import com.gitlab.d161tal.server.api.v1.Request
import com.gitlab.d161tal.server.service.RequestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/requests/{request}/attachments")
class RequestAttachmentController(@Autowired private val requestService: RequestService) {
    @PostMapping("/{attachment}")
    fun addAttachment(@PathVariable request: Request, @PathVariable attachment: FileAttachment): Request {
        request.attachments.add(attachment)
        requestService.add(request)
        return request
    }

    @DeleteMapping("/{attachment}")
    fun deleteAttachment(@PathVariable request: Request, @PathVariable attachment: FileAttachment) {
        request.attachments.remove(attachment)
        requestService.add(request)
    }
}
