package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.api.v1.FileAttachment
import com.gitlab.d161tal.server.api.v1.Request
import com.gitlab.d161tal.server.service.RequestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/requests/{request}/responsibles")
class RequestResponsibleController(@Autowired private val requestService: RequestService) {
    @PostMapping("/{responsibleId}")
    fun addResponsible(@PathVariable request: Request, @PathVariable responsibleId: Long): Request {
        request.responsible.add(responsibleId)
        requestService.add(request)
        return request
    }

    @DeleteMapping("/{responsibleId}")
    fun deleteResponsible(@PathVariable request: Request, @PathVariable responsibleId: Long) {
        request.responsible.remove(responsibleId)
        requestService.add(request)
    }
}
