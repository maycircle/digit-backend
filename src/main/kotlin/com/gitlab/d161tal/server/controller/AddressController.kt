package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.api.v1.Address
import com.gitlab.d161tal.server.api.v1.City
import com.gitlab.d161tal.server.dto.AddressDTO
import com.gitlab.d161tal.server.service.AddressService
import com.gitlab.d161tal.server.service.CityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/addresses")
class AddressController(
    @Autowired private val addressService: AddressService,
    @Autowired private val cityService: CityService
) {
    @GetMapping
    fun getAddresses() = addressService.list()

    @GetMapping("/{id}")
    fun getAddress(@PathVariable id: Long) = addressService.get(id)

    @PostMapping
    fun addAddress(@RequestBody address: AddressDTO): Address {
        val persistentAddress = Address().apply {
            city = cityService.get(address.city_id).get()
            street = address.street
            house = address.house
            entrance = address.entrance
            apartment = address.apartment
        }

        return addressService.add(persistentAddress)
    }
}
