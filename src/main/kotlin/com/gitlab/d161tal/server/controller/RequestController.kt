package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.dto.RequestDTO
import com.gitlab.d161tal.server.api.v1.Request
import org.springframework.beans.factory.annotation.Autowired
import com.gitlab.d161tal.server.service.RequestService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/requests")
class RequestController(@Autowired private val requestService: RequestService) {
    @GetMapping
    fun getRequests() = requestService.list()

    @GetMapping("/{id}")
    fun getRequest(@PathVariable id: Long) = requestService.get(id)

    @PostMapping
    fun addRequest(@RequestBody request: RequestDTO): Request {
        val persistentRequest = Request().apply {
            title = request.title
            description = request.description
            phoneNumber = request.phoneNumber
            requestType = request.requestType
            responsible = request.responsible?.toMutableSet() ?: mutableSetOf()
        }

        return requestService.add(persistentRequest)
    }

    @DeleteMapping("/{id}")
    fun deleteRequest(@PathVariable id: Request) = requestService.delete(id)
}
