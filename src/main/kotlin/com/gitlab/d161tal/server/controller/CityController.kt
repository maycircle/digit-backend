package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.api.v1.City
import com.gitlab.d161tal.server.dto.CityDTO
import com.gitlab.d161tal.server.service.CityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/cities")
class CityController(@Autowired private val cityService: CityService) {
    @GetMapping
    fun getCities() = cityService.list()

    @GetMapping("/{id}")
    fun getCity(@PathVariable id: Long) = cityService.get(id)

    @PostMapping
    fun addCity(@RequestBody city: CityDTO): City {
        val persistentCity = City().apply {
            name = city.name
        }

        return cityService.add(persistentCity)
    }
}
