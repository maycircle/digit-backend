package com.gitlab.d161tal.server.controller

import com.gitlab.d161tal.server.api.v1.FileAttachment
import com.gitlab.d161tal.server.service.FileAttachmentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class FileAttachmentController(@Autowired private val fileAttachmentService: FileAttachmentService) {
    @GetMapping("/v1/attachments/{attachment}")
    fun getAttachment(@PathVariable attachment: FileAttachment) = attachment

    @PostMapping("/v1/upload")
    fun uploadAttachment(@RequestParam name: String, @RequestParam file: MultipartFile): FileAttachment =
        fileAttachmentService.store(name, file)
}
