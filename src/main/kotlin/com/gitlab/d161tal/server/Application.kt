package com.gitlab.d161tal.server

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.MultipartConfigFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.util.unit.DataSize

import javax.servlet.MultipartConfigElement

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
class Application {
    @Bean
    fun multipartConfigElement(): MultipartConfigElement? {
        val factory = MultipartConfigFactory()
        factory.setMaxFileSize(DataSize.ofMegabytes(2))
        factory.setMaxRequestSize(DataSize.ofMegabytes(2))
        return factory.createMultipartConfig()
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
