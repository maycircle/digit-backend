package com.gitlab.d161tal.server.api.v1.initiative

enum class InitiativeType {
    VOTE {
        override fun create() = VoteInitiative()
    },
    EVENT {
        override fun create() = EventInitiative()
    },
    HELP {
        override fun create() = HelpInitiative()
    };

    abstract fun create(): InitiativeSpecs
}
