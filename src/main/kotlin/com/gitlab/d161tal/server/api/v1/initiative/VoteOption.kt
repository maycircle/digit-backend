package com.gitlab.d161tal.server.api.v1.initiative

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
open class VoteOption {
    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "vote_initiative_id")
    open var voteInitiative: InitiativeSpecs? = null

    @JsonProperty("option_name")
    @Column(name = "option_name", nullable = false)
    open var optionName: String? = null

    @ElementCollection
    @CollectionTable(name = "vote_option_participants", joinColumns = [JoinColumn(name = "vote_option_id")])
    @Column(name = "user_id")
    open var participants: MutableSet<Long> = mutableSetOf()
}
