package com.gitlab.d161tal.server.api.v1.initiative

import com.fasterxml.jackson.annotation.JsonInclude
import javax.persistence.*

@Entity
@DiscriminatorValue("VOTE")
@JsonInclude(JsonInclude.Include.NON_NULL)
open class VoteInitiative : InitiativeSpecs() {
    @OneToMany(mappedBy = "voteInitiative", cascade = arrayOf(CascadeType.ALL))
    open var voteOptions: MutableSet<VoteOption> = mutableSetOf()
}
