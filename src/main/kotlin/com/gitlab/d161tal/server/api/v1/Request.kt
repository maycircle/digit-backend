package com.gitlab.d161tal.server.api.v1

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
open class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @Column(name = "title", nullable = false)
    open var title: String? = null

    @JoinColumn(name = "description", nullable = false)
    open var description: String? = null

    @JsonProperty("phone_number")
    @Column(name = "phone_number", nullable = false)
    open var phoneNumber: Long? = null

    @Enumerated(EnumType.STRING)
    @JsonProperty("request_type")
    @Column(name = "request_type", nullable = false)
    open var requestType: RequestType? = null

    @ElementCollection
    @CollectionTable(name = "request_responsible", joinColumns = [JoinColumn(name = "request_id")])
    @Column(name = "user_id")
    open var responsible: MutableSet<Long> = mutableSetOf()

    @OneToMany(orphanRemoval = true, cascade = arrayOf(CascadeType.ALL))
    open var attachments: MutableSet<FileAttachment> = mutableSetOf()
}
