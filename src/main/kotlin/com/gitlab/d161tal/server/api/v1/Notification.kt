package com.gitlab.d161tal.server.api.v1

import java.sql.Timestamp
import javax.persistence.*

@Entity
open class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @Column(name = "title", nullable = false)
    open var title: String? = null

    @Column(name = "startDate", nullable = false)
    open var startDate: Timestamp? = null

    @Column(name = "endDate", nullable = true)
    open var endDate: Timestamp? = null

    @Column(name="description", nullable = true)
    open var description: String? = null
}