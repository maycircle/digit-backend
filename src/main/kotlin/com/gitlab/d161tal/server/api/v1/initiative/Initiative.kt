package com.gitlab.d161tal.server.api.v1.initiative

import java.sql.Timestamp
import javax.persistence.*

@Entity
open class Initiative {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @Column(name = "title", nullable = false)
    open var title: String? = null

    @Column(name = "description")
    open var description: String? = null

    @Column(name = "date")
    open var date: Timestamp? = null

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "initiative_specs_id", unique = true)
    open var initiativeSpecs: InitiativeSpecs? = null
}
