package com.gitlab.d161tal.server.api.v1

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.Hibernate
import javax.persistence.*

@Entity
open class FileAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @JsonIgnore
    @Column(name = "name")
    open var name: String? = null

    fun getFilename(): String? {
        return id.toString() + "-" + name
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as FileAttachment

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()
}
