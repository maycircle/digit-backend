package com.gitlab.d161tal.server.api.v1

enum class RequestType {
    PUBLIC, PRIVATE
}
