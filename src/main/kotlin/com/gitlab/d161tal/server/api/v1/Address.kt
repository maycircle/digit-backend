package com.gitlab.d161tal.server.api.v1

import javax.persistence.*

@Entity
open class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @ManyToOne(optional = false)
    @JoinColumn(name = "city_id", nullable = false)
    open var city: City? = null

    @Column(name = "street", nullable = false)
    open var street: String? = null

    @Column(name = "house")
    open var house: String? = null

    @Column(name = "entrance")
    open var entrance: String? = null

    @Column(name = "apartment")
    open var apartment: Int? = null
}
