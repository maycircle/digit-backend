package com.gitlab.d161tal.server.api.v1.initiative

import com.fasterxml.jackson.annotation.JsonInclude
import javax.persistence.*

@Entity
@DiscriminatorValue("EVENT")
@JsonInclude(JsonInclude.Include.NON_NULL)
open class EventInitiative : InitiativeSpecs()
