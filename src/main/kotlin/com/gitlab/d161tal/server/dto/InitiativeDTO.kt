package com.gitlab.d161tal.server.dto

import com.gitlab.d161tal.server.api.v1.initiative.InitiativeType
import java.sql.Timestamp

class InitiativeDTO(
    val type: InitiativeType,
    val title: String,
    val description: String?,
    val date: Timestamp?,
)
