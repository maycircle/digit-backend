package com.gitlab.d161tal.server.dto

class VoteDTO(
    val options: Set<String>
)
