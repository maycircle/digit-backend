package com.gitlab.d161tal.server.dto

import java.sql.Timestamp

class NotificationDTO(
    var title: String,
    val startDate: Timestamp,
    val endDate: Timestamp?,
    var description: String?
)