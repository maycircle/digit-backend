package com.gitlab.d161tal.server.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.gitlab.d161tal.server.api.v1.RequestType

class RequestDTO(
    val title: String,
    val description: String,
    @JsonProperty("phone_number")
    val phoneNumber: Long,
    @JsonProperty("request_type")
    val requestType: RequestType,
    val responsible: Set<Long>?
)
