package com.gitlab.d161tal.server.dto

class AddressDTO(
    val city_id: Long,
    val street: String,
    val house: String?,
    val entrance: String?,
    val apartment: Int?
)
