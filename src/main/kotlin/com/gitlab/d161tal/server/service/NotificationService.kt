package com.gitlab.d161tal.server.service

import com.gitlab.d161tal.server.api.v1.Notification
import com.gitlab.d161tal.server.repository.NotificationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class NotificationService(@Autowired private val notificationRepository: NotificationRepository) {
    fun list(): List<Notification> = notificationRepository.findAll()

    fun get(id: Long): Optional<Notification> = notificationRepository.findById(id)

    fun add(notification: Notification): Notification = notificationRepository.save(notification)
}