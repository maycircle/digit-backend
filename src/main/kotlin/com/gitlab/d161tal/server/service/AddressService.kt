package com.gitlab.d161tal.server.service

import com.gitlab.d161tal.server.api.v1.Address
import com.gitlab.d161tal.server.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class AddressService(@Autowired private val addressRepository: AddressRepository) {
    fun list(): List<Address> = addressRepository.findAll()

    fun get(id: Long): Optional<Address> = addressRepository.findById(id)

    fun add(address: Address): Address = addressRepository.save(address)
}
