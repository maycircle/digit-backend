package com.gitlab.d161tal.server.service.initiative

import com.gitlab.d161tal.server.api.v1.initiative.Initiative
import com.gitlab.d161tal.server.api.v1.initiative.InitiativeSpecs
import com.gitlab.d161tal.server.repository.initiative.InitiativeRepository
import com.gitlab.d161tal.server.repository.initiative.InitiativeSpecsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import java.util.*

@Service
class InitiativeService(@Autowired private val initiativeRepository: InitiativeRepository,
                        @Autowired private val initiativeSpecsRepository: InitiativeSpecsRepository
) {
    fun list(): List<Initiative> = initiativeRepository.findAll()

    fun get(id: Long): Optional<Initiative> = initiativeRepository.findById(id)

    fun add(initiative: Initiative): Initiative = initiativeRepository.save(initiative)
    fun addSpecs(initiativeSpecs: InitiativeSpecs): InitiativeSpecs = initiativeSpecsRepository.save(initiativeSpecs)

    fun remove(id: Long) = initiativeRepository.deleteById(id)
}
