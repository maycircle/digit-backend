package com.gitlab.d161tal.server.service.initiative

import com.gitlab.d161tal.server.api.v1.initiative.InitiativeSpecs
import com.gitlab.d161tal.server.api.v1.initiative.VoteInitiative
import com.gitlab.d161tal.server.api.v1.initiative.VoteOption
import com.gitlab.d161tal.server.repository.initiative.VoteInitiativeRepository
import com.gitlab.d161tal.server.repository.initiative.VoteOptionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class VoteInitiativeService(@Autowired private val voteInitiativeRepository: VoteInitiativeRepository,
                            @Autowired private val voteOptionRepository: VoteOptionRepository
) {
    fun list(): List<InitiativeSpecs> = voteInitiativeRepository.findAll()

    fun get(id: Long): Optional<VoteInitiative> = voteInitiativeRepository.findById(id)

    fun add(voteInitiative: VoteInitiative): VoteInitiative = voteInitiativeRepository.save(voteInitiative)
    fun addOption(voteOption: VoteOption): VoteOption = voteOptionRepository.save(voteOption)
}
