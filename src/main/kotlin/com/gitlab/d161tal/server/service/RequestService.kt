package com.gitlab.d161tal.server.service

import java.util.Optional
import com.gitlab.d161tal.server.api.v1.Request
import com.gitlab.d161tal.server.repository.RequestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RequestService(@Autowired private val requestRepository: RequestRepository) {
    fun list(): List<Request> = requestRepository.findAll()

    fun get(id: Long): Optional<Request> = requestRepository.findById(id)

    fun add(request: Request): Request = requestRepository.save(request)

    fun delete(request: Request) = requestRepository.delete(request)
}
