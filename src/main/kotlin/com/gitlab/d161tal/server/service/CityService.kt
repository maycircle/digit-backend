package com.gitlab.d161tal.server.service

import java.util.Optional
import com.gitlab.d161tal.server.api.v1.City
import com.gitlab.d161tal.server.repository.CityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CityService(@Autowired private val cityRepository: CityRepository) {
    fun list(): List<City> = cityRepository.findAll()

    fun get(id: Long): Optional<City> = cityRepository.findById(id)

    fun add(city: City): City = cityRepository.save(city)
}
