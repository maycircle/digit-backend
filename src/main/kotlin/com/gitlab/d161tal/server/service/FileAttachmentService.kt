package com.gitlab.d161tal.server.service

import com.gitlab.d161tal.server.api.v1.FileAttachment
import java.util.Optional
import com.gitlab.d161tal.server.repository.FileAttachmentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream

@Service
class FileAttachmentService(@Autowired private val fileAttachmentRepository: FileAttachmentRepository) {
    fun store(name: String, file: MultipartFile): FileAttachment {
        val persistentAttachment = fileAttachmentRepository.save(FileAttachment().apply {
            this.name = name
        })

        val bytes = file.bytes
        val stream = BufferedOutputStream(FileOutputStream(
            File(persistentAttachment.getFilename())
        ))

        stream.write(bytes)
        stream.close()

        return persistentAttachment
    }
}
