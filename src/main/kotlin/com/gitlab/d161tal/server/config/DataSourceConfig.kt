package com.gitlab.d161tal.server.config

import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class DataSourceConfig {
    @Bean
    fun getDataSource(): DataSource {
        val dataSourceBuilder = DataSourceBuilder.create()

        val host = System.getenv("POSTGRES_HOST") ?: "localhost"
        val username = System.getenv("POSTGRES_USER") ?: "postgres"
        val database = System.getenv("POSTGRES_DB") ?: username
        val password = System.getenv("POSTGRES_PASSWORD")

        return dataSourceBuilder
            .url("jdbc:postgresql://${host}:5432/${database}")
            .username(username)
            .password(password)
            .build()
    }
}
