package com.gitlab.d161tal.server.repository.initiative

import com.gitlab.d161tal.server.api.v1.initiative.VoteInitiative
import org.springframework.data.jpa.repository.JpaRepository

interface VoteInitiativeRepository : JpaRepository<VoteInitiative, Long>
