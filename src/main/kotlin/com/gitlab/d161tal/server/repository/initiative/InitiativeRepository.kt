package com.gitlab.d161tal.server.repository.initiative

import com.gitlab.d161tal.server.api.v1.initiative.Initiative
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InitiativeRepository : JpaRepository<Initiative, Long>
