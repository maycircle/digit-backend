package com.gitlab.d161tal.server.repository;

import com.gitlab.d161tal.server.api.v1.Request
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository

@Repository
interface RequestRepository : JpaRepository<Request, Long>
