package com.gitlab.d161tal.server.repository

import com.gitlab.d161tal.server.api.v1.Notification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface NotificationRepository : JpaRepository<Notification, Long>